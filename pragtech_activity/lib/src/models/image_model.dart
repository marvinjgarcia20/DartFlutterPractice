class ImageModel {
  final int? id;
  final String? title;
  final String? url;

  ImageModel({this.id, this.title, this.url});

  factory ImageModel.fromJson(Map<String, dynamic> jsonSource) => ImageModel(
        id: jsonSource['id'],
        title: jsonSource['title'],
        url: jsonSource['url'],
      );
}
