import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc_cubit/images_bloc.dart';
import '../bloc_cubit/images_count.dart';
import 'image_builder.dart';

class ImagesView extends StatelessWidget {
  final _controller = ScrollController();
  final countBloc = ImgCountBloc();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Images'),
      ),
      body: BlocConsumer<ImagesBloc, ImagesState>(
        listener: (context, state) {
          if (state.wasIncremented == true)
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text('Image added!'),
                duration: Duration(seconds: 1),
              ),
            );
        },
        builder: (context, state) {
          if (state is LoadingImagesState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is LoadedImagesState) {
            return RefreshIndicator(
              onRefresh: () async => BlocProvider.of<ImagesBloc>(context)
                  .add(PullToRefreshEvent()),
              child: ListView.builder(
                controller: _controller,
                itemCount: countBloc.state,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: buildImage(state.images![index]),
                    ),
                  );
                },
              ),
            );
          } else if (state is FailedToLoadImagesState) {
            return Center(
              child: Text('Error occured: ${state.error}'),
            );
          } else {
            return Container();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        //Adding more images
        child: Icon(Icons.add),
        onPressed: () async {
          countBloc.add(ImgCountEvent.increment);
          BlocProvider.of<ImagesBloc>(context).add(IncreaseImagesEvent());

          //Scroll to bottom
          Timer(
            Duration(seconds: 1),
            () => _controller.animateTo(
              _controller.position.maxScrollExtent,
              duration: Duration(seconds: 1),
              curve: Curves.fastOutSlowIn,
            ),
          );
        },
      ),
    );
  }
}
