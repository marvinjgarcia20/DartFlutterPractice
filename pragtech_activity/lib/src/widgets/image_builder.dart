import 'package:flutter/material.dart';
import 'package:pragtech_activity/src/models/image_model.dart';

Widget buildImage(ImageModel image) {
  return Container(
    margin: EdgeInsets.all(20.0),
    padding: EdgeInsets.all(20.0),
    decoration: BoxDecoration(
      border: Border.all(color: Colors.grey),
    ),
    child: Column(
      children: <Widget>[
        Padding(
          child: SizedBox(
            height: 300.0,
            child: Image.network(image.url!),
          ),
          padding: EdgeInsets.only(bottom: 20.0),
        ),
        Text(image.title!),
      ],
    ),
  );
}
