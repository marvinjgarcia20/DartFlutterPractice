import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';

enum ImgCountEvent { increment, decrement }

class ImgCountBloc extends Bloc<ImgCountEvent, int> {
  ImgCountBloc() : super(0);

  @override
  Stream<int> mapEventToState(ImgCountEvent event) async* {
    switch (event) {
      case ImgCountEvent.increment:
        yield state + 1;
        break;
      case ImgCountEvent.decrement:
        yield state - 1;
        break;
    }
  }
}
