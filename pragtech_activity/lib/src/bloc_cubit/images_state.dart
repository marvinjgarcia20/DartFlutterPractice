part of 'images_bloc.dart';

class ImagesState {
  bool wasIncremented = false;
}

class LoadingImagesState extends ImagesState {}

class LoadedImagesState extends ImagesState {
  List<ImageModel>? images;

  LoadedImagesState({this.images});
}

class FailedToLoadImagesState extends ImagesState {
  dynamic error;
  FailedToLoadImagesState({this.error});
}
