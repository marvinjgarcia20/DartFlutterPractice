import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';
import '../data/images_data.dart';
import '../models/image_model.dart';

part 'images_event.dart';
part 'images_state.dart';

class ImagesBloc extends Bloc<ImagesEvent, ImagesState> {
  final _data = ImageData();

  ImagesBloc() : super(LoadingImagesState());

  @override
  Stream<ImagesState> mapEventToState(ImagesEvent event) async* {
    if (event is LoadImagesEvent ||
        event is PullToRefreshEvent ||
        event is IncreaseImagesEvent) {
      yield LoadingImagesState();

      try {
        final images = await _data.fetchImages();
        yield LoadedImagesState(images: images);
      } catch (e) {
        yield FailedToLoadImagesState(error: e);
      }
    }

    if (event is IncreaseImagesEvent) state.wasIncremented = true;
  }
}
