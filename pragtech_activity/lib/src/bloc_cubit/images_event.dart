part of 'images_bloc.dart';

abstract class ImagesEvent {}

class LoadImagesEvent extends ImagesEvent {}

class PullToRefreshEvent extends ImagesEvent {}

class IncreaseImagesEvent extends ImagesEvent {}
