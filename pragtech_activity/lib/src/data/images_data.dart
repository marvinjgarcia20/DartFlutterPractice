import 'dart:convert';
import 'package:http/http.dart' show get;
import '../models/image_model.dart';

class ImageData {
  final _root = 'jsonplaceholder.typicode.com';

  Future<List<ImageModel>> fetchImages() async {
    try {
      final uri = Uri.https(_root, '/photos');
      final response = await get(uri);
      final json = jsonDecode(response.body) as List;
      final images =
          json.map((postJson) => ImageModel.fromJson(postJson)).toList();
      return images;
    } catch (e) {
      throw e;
    }
  }
}
