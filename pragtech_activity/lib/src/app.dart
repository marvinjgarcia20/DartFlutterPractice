import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pragtech_activity/src/widgets/images_view.dart';
import 'bloc_cubit/images_bloc.dart';

class App extends StatefulWidget {
  State<StatefulWidget> createState() => _AppState();
}

class _AppState extends State<App> {
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider<ImagesBloc>(
        create: (_) => ImagesBloc()..add(LoadImagesEvent()),
        child: ImagesView(),
      ),
    );
  }
}
