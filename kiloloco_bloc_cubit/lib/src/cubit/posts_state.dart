part of 'posts_bloc.dart';

abstract class PostsState {}

class LoadingPostsState extends PostsState {}

class LoadedPostsState extends PostsState {
  List<PostModel>? posts;

  LoadedPostsState({this.posts});
}

class FailedToLoadPostsState extends PostsState {
  dynamic error;
  FailedToLoadPostsState({this.error});
}
