class PostModel {
  final int? userId;
  final int? id;
  final String? title;
  final String? body;

  PostModel({this.userId, this.id, this.title, this.body});

  factory PostModel.fromJson(Map<String, dynamic> jsonSource) => PostModel(
        userId: jsonSource['userId'],
        id: jsonSource['id'],
        title: jsonSource['title'],
        body: jsonSource['body'],
      );
}
