import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/post_model.dart';

class DataService {
  final _baseUrl = 'jsonplaceholder.typicode.com';

  Future<List<PostModel>> getPosts() async {
    try {
      final uri = Uri.https(_baseUrl, '/posts');
      final response = await http.get(uri);
      final json = jsonDecode(response.body) as List;
      final posts =
          json.map((postJson) => PostModel.fromJson(postJson)).toList();
      return posts;
    } catch (e) {
      throw e;
    }
  }
}
